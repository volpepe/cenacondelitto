public class Executor {
	public static void executeCommand(final String s) {
		try {
			final Process p = Runtime.getRuntime().exec(s);
			p.waitFor();
		} catch (Exception e) {
			System.err.println("oops... call your masters");
			e.printStackTrace();
		}
	}
}
