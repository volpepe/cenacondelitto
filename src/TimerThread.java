public class TimerThread extends Thread {
	
	private boolean isDead;
	private final int THREADLIFETIME;
	private final String whatToDoCommand;
	
	public TimerThread(final int threadLifeTime, final String whatToDo) {
		super();
		this.THREADLIFETIME = threadLifeTime;
		this.isDead = false;
		this.whatToDoCommand = whatToDo;
	}
	
	public void run() {
		System.out.println("Inserting the DEATH PASSWORD will stop this counter. There's only one chance to insert it correctly.");
		System.out.println("Come back in: ");
		for (int time = THREADLIFETIME; time > 0; time--) {
			String showSeconds = Integer.toString(time % 60);
			String showMinutes = Integer.toString(time / 60);
			if (showSeconds == "0") {
				showSeconds = "00";
			}
			else if (showSeconds.length() == 1) {
				showSeconds = "0" + showSeconds;
			}
			if (showMinutes.length() == 1) {
				showMinutes = "0" + showMinutes;
			}
			System.out.print("\r" + showMinutes + ":" + showSeconds); 
			try {
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.err.println("oops... call your masters");
				e.printStackTrace();
			}
			System.out.flush();
			if(isDead) {
				break;
			}
		}
		if (!isDead) {
			System.out.println("\nAccess Granted!");
			Executor.executeCommand(whatToDoCommand);
		}
	}
	
	public void kill() {
		this.isDead = true;
	}
}
