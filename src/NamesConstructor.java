import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class NamesConstructor {
	
	private final List<String> list;
	
	public NamesConstructor() {
		list = new ArrayList<>(Arrays.asList(new String[] {
				"dry thread", "drought", "drifting herald", "drum crowd", "dragon doomed",
				"drunken guard", "dripping blood", "death stranding", "dream's hand", "drugbound",
				"drama squid", "dreamhack", "dr ward", "dr. ward", "drippin oil", "defeated soul",
				"doors of doom", "dreamnet", "ants", "dreadful mind", "dreamund", "legend of dreamund",
				"reborn", "dropped", "galactic gore", "rapid fire", "drowned", "domestic abuse"
				//others?
		}));
	}
	
	public List<String> build() {
		return Collections.unmodifiableList(list);
	}

}
