import java.util.List;
import java.util.Scanner;

public class Launcher {
	
	private static final String PASS = "dreamshard";
	private static final String KILL = "ec7y9gow";
	private static final int THREADLIFETIME = 240;	//240 seconds = 4 minutes
	private static final String OPENCOMMAND = "chromium-browser https://imgur.com/a/FLa7iDx";	//open file
	
	private static int remainingChances = 3;
	
	private static final NamesConstructor n = new NamesConstructor();
	
	public static void main(String[] args) {
		Scanner reader = new Scanner(System.in);
		String userString = new String();
		boolean stop = false;
		//this particular code clears the console before starting
		System.out.print("\033[H\033[2J");
		System.out.flush();
		final List<String> list = n.build();
		while (!stop && remainingChances > 0) {
			System.out.println("Enter Password (you can still try " + remainingChances + " time/s)");
			userString = reader.nextLine().toLowerCase();
			if (userString.equals(PASS)) {
				//access granted!
				final TimerThread t = new TimerThread(THREADLIFETIME, OPENCOMMAND);
				
				//whoever knows the killing password only has one try.
				final Thread get = new Thread(() -> {
					char[] killMe = System.console().readPassword();
					String killString = new String();
					for (final char k : killMe) {
						killString += k;
					}
					if (killString.equals(KILL)) {
						t.kill();
						System.out.println("The timer has been stopped");
						removeFile();
					} else {
						System.out.println("Someone has been trying to input a wrong termination password. Stay alert.");
						System.out.println("DEATH PASSWORD has been disabled.");
					}
				});	
				t.start();
				get.start();
				try {
					t.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
				stop = true;
				try {
					get.join();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			else if (userString.equals(KILL)) {
				removeFile();
				stop = true;
			}
			else {
				if (list.contains(userString)) {
					remainingChances--;
				}
				System.out.println("Wrong!");
			}
			System.out.println("");
		}
		if (remainingChances == 0) {
			removeFile();
		}
		reader.close();
	}
	
	private static void removeFile() {
		System.out.println("All files have been eliminated.");
	}
}
